package com.company;

/**
 * @author giovanni.stevanato
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class TaskHandler implements Runnable {                                                                          //TaskHandler Class which handle a single connection with a client

    private final Socket socket;                                                                                        //Client TCP Socket
    private final BufferedReader inFromClient;                                                                          //INPUT from the client
    private final DataOutputStream outToClient;                                                                         //OUTPUT to the client
    private final int CLIENT_NUMBER;                                                                                    //Final variable which contains this client number

    TaskHandler(Socket socket, int clientNumber) throws IOException {                                                   //TaskHandler constructor

        this.socket = socket;                                                                                           //Get the socket.accept() from the server and set it
        this.CLIENT_NUMBER = clientNumber;                                                                              //Get the clientNumber from the server and set it
        this.inFromClient = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));                    //Create and set the INPUT stream
        this.outToClient = new DataOutputStream(this.socket.getOutputStream());                                         //Create and set the OUTPUT stream

    }

    public synchronized void sendMessage(String message) {                                                              //Sends the String 'message' to the client

        try {
            this.outToClient.writeBytes(message + "\n");
            this.outToClient.flush();
        } catch (IOException e) {}

    }

    public synchronized String getMessage() {                                                                           //Returns a String containing the message sent from the client

        String message = null;

        try {
            message = this.inFromClient.readLine();
        } catch (IOException e) {}

        return message;

    }

    public void myLog(String m) {                                                                                       //MyLog message with the client number

        System.out.println("CLIENT #" + this.CLIENT_NUMBER + " " + m);

    }

    @Override
    public void run()                                                                                                   //Actual client communication handling
    {

        myLog("CONNECTED");                                                                                         //START OF THE COMMUNICATION

        String lastMessage = "";

        try{

            while(!lastMessage.equals("QUIT")){                                                                         //While client user doesn't type `QUIT` keep communicating

                sendMessage("HELLO CLIENT #"+ this.CLIENT_NUMBER +" ---> Type in a message and press return to send it to the server (type `QUIT` to end communication):");
                                                                                                                        //Sending a message to the client
                lastMessage = getMessage();                                                                             //Receiving a message from the client

                if(lastMessage != null){
                    myLog(" > \"" + lastMessage + "\"");                                                            //Printing the message received
                }
            }

        }catch(NullPointerException e){                                                                                 //Catching a connection drop
            myLog(" << ERROR: connection lost >>");
        }

        myLog("DISCONNECTED");                                                                                      //END OF THE COMMUNICATION

    }
}
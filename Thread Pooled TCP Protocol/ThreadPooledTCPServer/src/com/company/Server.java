package com.company;

/**
 *
 * @author giovanni.stevanato
 */

import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {                                                                               //Server class who can be ran

    private final ServerSocket serverSocket;                                                                            //Server TCP Socket
    private final ExecutorService pool;                                                                                 //Pool of Executors
    private final int port;                                                                                             //Port Number
    private static int clientNumber = 1;                                                                                //Variable which keeps track of how many clients have been connected

    public Server (int port, int poolSize) throws IOException {                                                         //Server class constructor

        this.port = port;                                                                                               //Set the port
        serverSocket = new ServerSocket (this.port);                                                                    //Instantiation of the serverSocket
        pool = Executors.newFixedThreadPool (poolSize);                                                                 //Instantiation of the ThreadPool and setting the pool size

    }

    public static synchronized int getOrdinationNumber(){                                                               //Return the value of clientNumber and increments it
        return clientNumber++;
    }

    @Override
    public void run () {                                                                                                //Server Runs and start listening for clients who connects

        System.out.println("THE SERVER IS NOW RUNNING\nWaiting for clients to connect...");

        try {
            while(true){
                pool.execute (new TaskHandler(serverSocket.accept (), getOrdinationNumber()));                          //Each time a client connects create a TaskHandler and runs it in the thread pool
            }
        } catch (IOException ex) {
            System.out.println("THE SERVER IS NOW CLOSING");
            pool.shutdown ();                                                                                           //Shutting down the server
        }

        System.out.println("THE SERVER IS CLOSED");
    }

}
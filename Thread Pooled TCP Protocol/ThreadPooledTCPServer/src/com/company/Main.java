package com.company;

/**
 *
 * @author giovanni.stevanato
 */

public class Main {
    public static void main (String [] args) throws Exception{

        Server s = new Server (9999, 5);                                                                   //Creating a Thread Pooled TCP Server on the port 9999 and with a pool size of 5

        s.run ();                                                                                                       //Running the Server

    }
}
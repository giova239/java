package com.company;

/**
 *
 * @author giovanni.stevanato
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){


        String lastMessage = "";
        Client client = null;
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));                                 //BufferedReader for the keyboard input

        try {
            System.out.println("Establishing connection. Please wait ...");
            client = new Client();                                                                                      //Instantiation of a Client class object
        } catch (IOException e) {
            System.out.println("unable to connect to the server");
            System.exit(1);                                                                                      //Server is offline (exiting with a error, status=1)
        }

        while(!lastMessage.equals("QUIT")){                                                                             //While user doesn't write `QUIT` keep communicate with the server

            System.out.println("S: " + client.getMessage());                                                            //Get and print the server Message

            try {
                lastMessage = keyboard.readLine();                                                                      //Waits for user to type in something...
            } catch (IOException e) {
                e.printStackTrace();
            }

            client.sendMessage(lastMessage);                                                                            //Sends it to the server

        }

        System.exit(0);                                                                                          //Connection has ended (exiting without a error, status=0)
    }
}

package com.company;

/**
 *
 * @author giovanni.stevanato
 */

import java.io.*;
import java.net.Socket;

public class Client {                                                                                                   //Client class allows you to easily handle comunication with the server

    private final int port = 9999;                                                                                      //Socket port is 9999
    private final String ip = "127.0.0.1";                                                                              //Socket ip is 127.0.0.1 (localhost)
    private final Socket socket;                                                                                        //Client TCP Socket
    private final BufferedReader inFromServer;                                                                          //INPUT from the server
    private final DataOutputStream outToServer;                                                                         //OUTPUT to the server

    public Client() throws IOException {                                                                                //Client class costructor

        this.socket = new Socket(this.ip,this.port);                                                                    //Create and set the Socket ip and port

        this.inFromServer = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));                    //Create and set the INPUT STREAM

        this.outToServer = new DataOutputStream(this.socket.getOutputStream());                                         //Create and set the OUTPUT STREAM

    }

    public String getMessage(){                                                                                         //Returns a String containing the message sent from the server

        String message = null;

        try {
            message = this.inFromServer.readLine();
        } catch (IOException e) {
            System.out.println("ERROR: IOException while getting the message");
        }
        return message;

    }

    public void sendMessage(String message){                                                                            //Sends the String 'message' to the server

        try {
            this.outToServer.writeBytes(message + "\n");
            this.outToServer.flush();
        } catch (IOException e) {
            System.out.println("ERROR: IOException while sending the message");
        }

    }

}
